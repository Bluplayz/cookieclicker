package de.csbme.packet.packets;

import lombok.Getter;

import java.io.Serializable;
import java.util.List;

public class TopTenPacket implements Serializable {

    @Getter // rank,username,cookies
    private List<String[]> players;

    /**
     * Will be sent from MasterServer to Client
     * with the top ten players with the most cookies
     */
    public TopTenPacket( List<String[]> players ) {
        this.players = players;
    }
}
