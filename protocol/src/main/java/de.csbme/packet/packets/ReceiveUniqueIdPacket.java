package de.csbme.packet.packets;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

public class ReceiveUniqueIdPacket implements Serializable {

    @Getter
    private UUID player;

    /**
     * Will be sent from MasterServer to Client
     * when the player is new and has no unique id.
     * The MasterServer generate a id and send it to the client
     */
    public ReceiveUniqueIdPacket( UUID player ) {
        this.player = player;
    }
}
