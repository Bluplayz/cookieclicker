package de.csbme.packet.packets;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

public class ClientLoginPacket implements Serializable {

    @Getter
    private UUID player;

    @Getter
    private String username;

    @Getter
    private long cookies;

    /**
     * Will be sent from Client to MasterServer
     * when the player logged in with the cookies and username
     */
    public ClientLoginPacket( UUID player, String username, long cookies ) {
        this.player = player;
        this.username = username;
        this.cookies = cookies;
    }
}
