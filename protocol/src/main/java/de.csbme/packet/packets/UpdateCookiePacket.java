package de.csbme.packet.packets;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

public class UpdateCookiePacket implements Serializable {

    @Getter
    private UUID player;

    @Getter
    private long cookies;

    /**
     * Will be sent from Client to MasterServer
     * with the cookies of the player
     */
    public UpdateCookiePacket( UUID player, long cookies ) {
        this.player = player;
        this.cookies = cookies;
    }
}
