package de.csbme.packet.packets;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

public class UpdateUsernamePacket implements Serializable {

    @Getter
    private UUID player;

    @Getter
    private String username;

    public UpdateUsernamePacket( UUID player, String username ) {
        this.player = player;
        this.username = username;
    }
}
