package de.csbme.packet;

import java.io.Serializable;

public enum PacketType implements Serializable {

    CLIENT_LOGIN_PACKET,
    RECEIVE_UNIQUEID_PACKET,
    TOP_TEN_PACKET,
    UPDATE_COOKIE_PACKET,
    UPDATE_USERNAME_PACKET;
}
