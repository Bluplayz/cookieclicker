package de.csbme.packet;

import lombok.Getter;

import java.io.Serializable;

public class InitPacket implements Serializable {

    @Getter
    private PacketType packetType;

    @Getter
    private Object packet;

    public InitPacket( PacketType packetType, Object packet ) {
        this.packetType = packetType;
        this.packet = packet;
    }
}
