package de.csbme.window;

import de.csbme.game.Game;
import de.csbme.image.ImageLoader;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import lombok.Getter;

public class Window {

    public static final String TITLE = "Cookie Clicker";

    public static final int SCALE = 70;
    public static final int WIDTH = 14 * Window.SCALE;
    public static final int HEIGHT = 9 * Window.SCALE;

    public static final boolean DEBUGMODE = true;

    @Getter
    private Game game;

    @Getter
    private final Stage primaryStage;

    @Getter
    private static Window instance;

    @Getter
    private final Scene scene;

    @Getter
    private final Pane root;

    public Window( Stage primaryStage ) {
        this.primaryStage = primaryStage;
        Window.instance = this;

        // Set Window Title
        this.getPrimaryStage().setTitle( Window.TITLE );

        // Set Size
        this.getPrimaryStage().setWidth( Window.WIDTH );
        this.getPrimaryStage().setHeight( Window.HEIGHT );

        // Allow Resizing while Debugging
        this.getPrimaryStage().setResizable( Window.DEBUGMODE );

        // Set Icon
        this.getPrimaryStage().getIcons().add( ImageLoader.load( "Cookie.png" ) );

        // Create Root Pane
        this.root = new Pane();

        // Create Scene
        this.scene = new Scene( this.getRoot() );

        this.getRoot().setBackground( new Background( new BackgroundImage( ImageLoader.load( "Background.png" ),
                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT ) ) );

        // Set Scene
        this.getPrimaryStage().setScene( this.getScene() );

        // Handle Window Close at our own
        this.getPrimaryStage().setOnCloseRequest( event -> System.exit( 0 ) );

        // Init Game
        this.game = new Game( this );

        // Show Window
        this.getPrimaryStage().show();
    }
}
