package de.csbme.game.object;

import lombok.Getter;
import lombok.Setter;

import java.util.concurrent.atomic.AtomicInteger;

public class GameObject {

    private static AtomicInteger staticId = new AtomicInteger( 0 );

    @Getter
    private int id = 0;

    @Getter
    @Setter
    private String name = "";

    @Getter
    @Setter
    private boolean visible = true;

    public GameObject() {
        this.id = staticId.incrementAndGet();
    }
}
