package de.csbme.game.object;

import com.sun.javafx.geom.Ellipse2D;
import de.csbme.game.Game;
import de.csbme.image.ImageLoader;
import de.csbme.window.Window;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import lombok.Getter;

public class CookieObject extends GameObject {

    private static final int SIZE = 300;

    @Getter
    private final Ellipse2D ellipse2D = new Ellipse2D( Window.WIDTH / 2, Window.HEIGHT / 2 - ( SIZE / 2 ), SIZE, SIZE );

    @Getter
    private final Ellipse2D ellipse2DMinimized = new Ellipse2D( (float) ( Window.WIDTH / 2 + ( this.getEllipse2D().getWidth() - ( SIZE / 1.4 ) ) / 2 ), (float) ( Window.HEIGHT / 2 - ( SIZE / 2 ) + ( this.getEllipse2D().getWidth() - ( SIZE / 1.4 ) ) / 2 ), (float) ( SIZE - ( SIZE - ( SIZE / 1.4 ) ) ), (float) ( SIZE - ( SIZE - ( SIZE / 1.4 ) ) ) );

    @Getter
    private final Image image = ImageLoader.load( "Cookie.png" );

    @Getter
    private Button button = new Button( "Cookie" );

    private boolean pressed = false;

    public CookieObject() {
        this.getButton().setOnMousePressed( this::onMousePressed );
        this.getButton().setOnMouseReleased( this::onMouseReleased );
        this.update();
        Window.getInstance().getRoot().getChildren().add( this.getButton() );
    }

    private void update() {
        ImageView cookieImage = new ImageView( this.getImage() );
        if ( this.pressed ) {
            this.getButton().setLayoutX( this.getEllipse2DMinimized().getX() );
            this.getButton().setLayoutY( this.getEllipse2DMinimized().getY() );
            cookieImage.setFitWidth( this.getEllipse2DMinimized().getWidth() );
            cookieImage.setFitHeight( this.getEllipse2DMinimized().getHeight() );
            this.getButton().setStyle(
                    "-fx-background-radius: 50em; " +
                            "-fx-min-width: " + ( this.getEllipse2DMinimized().getWidth() ) + "px; " +
                            "-fx-min-height: " + ( this.getEllipse2DMinimized().getHeight() ) + "px; " +
                            "-fx-max-width: " + ( this.getEllipse2DMinimized().getWidth() ) + "px; " +
                            "-fx-max-height: " + ( this.getEllipse2DMinimized().getHeight() ) + "px;" +
                            "-fx-background-color: transparent;"
            );
        } else {
            this.getButton().setLayoutX( this.getEllipse2D().getX() );
            this.getButton().setLayoutY( this.getEllipse2D().getY() );
            cookieImage.setFitWidth( this.getEllipse2D().getWidth() );
            cookieImage.setFitHeight( this.getEllipse2D().getHeight() );
            this.getButton().setStyle(
                    "-fx-background-radius: 50em; " +
                            "-fx-min-width: " + ( this.getEllipse2D().getWidth() ) + "px; " +
                            "-fx-min-height: " + ( this.getEllipse2D().getHeight() ) + "px; " +
                            "-fx-max-width: " + ( this.getEllipse2D().getWidth() ) + "px; " +
                            "-fx-max-height: " + ( this.getEllipse2D().getHeight() ) + "px;" +
                            "-fx-background-color: transparent;"
            );
        }

        this.getButton().setGraphic( cookieImage );
    }

    public void onMousePressed( MouseEvent event ) {
        if ( event.getButton() != MouseButton.PRIMARY ) {
            return;
        }

        if ( !this.getEllipse2D().contains( (float) event.getSceneX(), (float) event.getSceneY() ) ) {
            return;
        }

        this.pressed = true;
        this.update();
    }

    public void onMouseReleased( MouseEvent event ) {
        if ( event.getButton() != MouseButton.PRIMARY ) {
            return;
        }

        this.pressed = false;
        this.update();

        if ( this.getEllipse2D().contains( (float) event.getSceneX(), (float) event.getSceneY() ) ) {
            Game.getInstance().getPlayer().setCookies( Game.getInstance().getPlayer().getCookies() + Game.getInstance().getPlayer().getCookiesPerClick() );
            Game.getInstance().getCookieCounterObject().update();
        }
    }
}
