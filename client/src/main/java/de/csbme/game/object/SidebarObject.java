package de.csbme.game.object;

import de.csbme.game.Game;
import de.csbme.image.ImageLoader;
import de.csbme.player.Player;
import de.csbme.window.Window;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;
import lombok.Getter;

import java.util.LinkedHashMap;

public class SidebarObject extends GameObject {

    private static final int WIDTH = Window.WIDTH / 3;
    private static final int HEIGHT = Window.HEIGHT;

    private static final int FIELD_WIDTH = WIDTH - 45;
    private static final int FIELD_HEIGHT = 100;
    @Getter
    private final Pane pane = new Pane();
    @Getter
    private Image image;
    @Getter
    private boolean open = false;

    @Getter
    private boolean animation = false;

    @Getter
    private int scrollOffset = 0;

    @Getter
    private LinkedHashMap<String, Pane> topPlayers = new LinkedHashMap<>();

    @Getter
    private Rectangle buttonRectangle = new Rectangle( 300, 280, 22, 66 );

    public SidebarObject() {
        this.getPane().setMinWidth( WIDTH );
        this.getPane().setMaxWidth( WIDTH );
        this.getPane().setMinHeight( HEIGHT );
        this.getPane().setMaxHeight( HEIGHT );

        if ( this.isOpen() ) {
            this.image = ImageLoader.load( "sidebar/opened-sidebar.png" );
            this.getPane().translateXProperty().setValue( 0 );
            this.getPane().translateYProperty().setValue( 0 );
        } else {
            this.image = ImageLoader.load( "sidebar/closed-sidebar.png" );
            this.getPane().translateXProperty().setValue( 0 - this.getPane().getMinWidth() + 18 );
            this.getPane().translateYProperty().setValue( 0 );
        }

        this.getPane().setBackground( new Background( new BackgroundImage( this.getImage(),
                BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT ) ) );

        this.getPane().setOnMousePressed( this::onMousePressed );
        this.getPane().setOnScroll( this::setOnScroll );
        Window.getInstance().getRoot().getChildren().add( this.getPane() );

//        this.loadTopPlayers();
    }

    public void loadTopPlayers() {
        Platform.runLater( () -> {
            for ( Pane pane : this.getTopPlayers().values() ) {
                this.getPane().getChildren().remove( pane );
            }
            this.getTopPlayers().clear();
            /*
            LinkedList<String> players = new LinkedList<String>() {{
                this.add( "6ceebf56-851d-48d9-99d8-82e900453b7b" );
                this.add( "82e900453b7b-851d-48d9-99d8-6ceebf56" );
                this.add( "48d9-82e900453b7b-6ceebf56-99d8-851d" );
                this.add( "Player4" );
                this.add( "Player5" );
                this.add( "Player6" );
                this.add( "Player7" );
                this.add( "Player8" );
                this.add( "Player9" );
                this.add( "Player10" );
            }};
            */

            boolean colorChange = false;

            int i = 0;
            for ( Player player : Game.getInstance().getTopTenPlayers() ) {
                Pane field = new Pane();
                field.setStyle(
                        "-fx-background-color: " + ( colorChange ? "black" : "white" ) + ";"
                );
                colorChange = !colorChange;

                Text text = new Text(
                        "Platz: " + ( i + 1 ) + "\n" +
                                "Username: " + player.getUsername() + "\n" +
                                "Cookies: " + player.getCookies()
                );
//                Text text = new Text( "Id: " + i );
                text.setLayoutY( 20 );
                text.setFill( Color.RED );
                text.setStyle( "-fx-font-size: 16;" );
                field.getChildren().add( text );
                this.getPane().getChildren().add( field );
                this.topPlayers.put( player.getUsername(), field );

                i++;
            }

            this.recalculateSidebar();
        } );
    }

    private void recalculateSidebar() {
        int topFieldId = ( FIELD_HEIGHT * this.getTopPlayers().size() - scrollOffset - Window.HEIGHT - 70 ) / 100 - 3;

        /*
        if ( topFieldId < 0 ) {
            return;
        }
        */

        // Top Player Fields
        int i = 0;
        for ( Pane field : this.getTopPlayers().values() ) {
            field.setLayoutX( 10 );
            field.setPrefSize( FIELD_WIDTH, FIELD_HEIGHT );

            if ( topFieldId == i ) {
                if ( topFieldId > 0 ) {
                    ( (Pane) this.getTopPlayers().values().toArray()[topFieldId - 1] ).setPrefSize( 0, 0 );
                }

                field.setLayoutY( 10 );
                i++;
                continue;
            }

            int y = i == 0 ? 10 : FIELD_HEIGHT * i;
            field.setLayoutY( y + this.getScrollOffset() );

            i++;
        }
    }

    private void setOnScroll( ScrollEvent scrollEvent ) {
        if ( this.getTopPlayers().size() < 6 ) {
            return;
        }

        if ( scrollEvent.getDeltaY() > 0 ) {
            scrollOffset += 20;
        } else {
            scrollOffset -= 20;
        }

        int topFieldId = ( FIELD_HEIGHT * this.getTopPlayers().size() - scrollOffset - Window.HEIGHT - 70 ) / 100 - 3;
        if ( topFieldId < 0 ) {
            scrollOffset = 0;
            this.recalculateSidebar();
            return;
        }

        //scrollOffset = scrollOffset < 0 ? 0 : scrollOffset;

        if ( this.getTopPlayers().size() > 5 ) {
            int maxY = Window.HEIGHT - 20;
            int y = FIELD_HEIGHT * this.getTopPlayers().size() + this.getScrollOffset() + 20;

            scrollOffset = y < maxY ? scrollOffset + 20 : scrollOffset;
        }

        this.recalculateSidebar();
    }

    private void onMousePressed( MouseEvent mouseEvent ) {
        if ( this.isAnimation() || mouseEvent.getButton() != MouseButton.PRIMARY ) {
            return;
        }

        Timeline timeline = new Timeline();
        if ( this.isOpen() ) {
            if ( !this.getButtonRectangle().contains( mouseEvent.getX(), mouseEvent.getY() ) ) {
                return;
            }

            timeline.getKeyFrames().addAll(
                    new KeyFrame( Duration.ZERO,
                            new KeyValue( this.getPane().translateXProperty(), this.getPane().translateXProperty().getValue() )
                    ),
                    new KeyFrame( new Duration( 200 ),
                            new KeyValue( this.getPane().translateXProperty(), 0 - this.getPane().getWidth() + 18 )
                    )
            );

            this.animation = true;
            timeline.play();
            timeline.setOnFinished( event -> {
                this.animation = false;
                this.open = false;

                this.image = ImageLoader.load( "sidebar/closed-sidebar.png" );
                this.getPane().setBackground( new Background( new BackgroundImage( this.getImage(),
                        BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT ) ) );
            } );
        } else {
            if ( !this.getButtonRectangle().contains( mouseEvent.getX(), mouseEvent.getY() ) ) {
                return;
            }

            timeline.getKeyFrames().addAll(
                    new KeyFrame( Duration.ZERO,
                            new KeyValue( this.getPane().translateXProperty(), this.getPane().translateXProperty().getValue() )
                    ),
                    new KeyFrame( new Duration( 200 ),
                            new KeyValue( this.getPane().translateXProperty(), 0 )
                    )
            );

            this.animation = true;
            timeline.play();
            timeline.setOnFinished( event -> {
                this.animation = false;
                this.open = true;

                this.image = ImageLoader.load( "sidebar/opened-sidebar.png" );
                this.getPane().setBackground( new Background( new BackgroundImage( this.getImage(),
                        BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT ) ) );
            } );
        }
    }
}
