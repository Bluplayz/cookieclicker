package de.csbme.game.object;

import de.csbme.game.Game;
import de.csbme.window.Window;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import lombok.Getter;

import java.text.NumberFormat;

public class CookieCounterObject extends GameObject {

    @Getter
    private Text cookieCounterText = new Text( "Loading..." ) {{
        Font.loadFont( this.getClass().getClassLoader().getResourceAsStream( "font/kavoon.otf" ), 5 );
        this.setFont( Font.font( "kavoon", FontWeight.BOLD, 30 ) );
    }};

    public CookieCounterObject() {
        this.update();
        Window.getInstance().getRoot().getChildren().add( this.getCookieCounterText() );
    }

    public void update() {
        String cookies = NumberFormat.getInstance().format( Game.getInstance().getPlayer().getCookies() );

        this.getCookieCounterText().setText( cookies + " Cookies" );
        this.getCookieCounterText().setX( Game.getInstance().getCookieObject().getEllipse2D().getCenterX() - 50 - ( this.getCookieCounterText().getFont().getSize() / 3 ) * String.valueOf( Game.getInstance().getPlayer().getCookies() ).length() );
        this.getCookieCounterText().setY( Game.getInstance().getCookieObject().getEllipse2D().getCenterY() - Game.getInstance().getCookieObject().getEllipse2D().getHeight() / 1.3 );
    }
}
