package de.csbme.game.object;

import com.sun.javafx.geom.Ellipse2D;
import de.csbme.image.ImageLoader;
import de.csbme.window.Window;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import lombok.Getter;

public class SettingsObject extends GameObject {

    private static final int SIZE = 80;

    @Getter
    private final Ellipse2D ellipse2D = new Ellipse2D( Window.WIDTH - SIZE - 40, Window.HEIGHT - SIZE - 50, SIZE, SIZE );

    @Getter
    private final Image image = ImageLoader.load( "icon/settings.png" );

    @Getter
    private Button button = new Button( "Settings" );

    private boolean pressed = false;

    public SettingsObject() {
        this.getButton().setOnMousePressed( this::onMousePressed );
        this.getButton().setOnMouseReleased( this::onMouseReleased );

        ImageView settingsImage = new ImageView( this.getImage() );
        this.getButton().setLayoutX( this.getEllipse2D().getX() );
        this.getButton().setLayoutY( this.getEllipse2D().getY() );
        settingsImage.setFitWidth( this.getEllipse2D().getWidth() );
        settingsImage.setFitHeight( this.getEllipse2D().getHeight() );
//        settingsImage.setTranslateX( 0 - ( this.getEllipse2D().getWidth() / 2 ) );
        this.getButton().setStyle(
                "-fx-background-radius: 50em; " +
                        "-fx-min-width: " + ( this.getEllipse2D().getWidth() ) + "px; " +
                        "-fx-min-height: " + ( this.getEllipse2D().getHeight() ) + "px; " +
                        "-fx-max-width: " + ( this.getEllipse2D().getWidth() ) + "px; " +
                        "-fx-max-height: " + ( this.getEllipse2D().getHeight() ) + "px;" +
                        "-fx-background-color: transparent;"
        );
        this.getButton().setGraphic( settingsImage );

        Window.getInstance().getRoot().getChildren().add( this.getButton() );
    }

    public void onMousePressed( MouseEvent event ) {
        if ( event.getButton() != MouseButton.PRIMARY ) {
            return;
        }

        if ( !this.getEllipse2D().contains( (float) event.getSceneX(), (float) event.getSceneY() ) ) {
            return;
        }

        this.pressed = true;
    }

    public void onMouseReleased( MouseEvent event ) {
        if ( event.getButton() != MouseButton.PRIMARY ) {
            return;
        }

        this.pressed = false;

        if ( this.getEllipse2D().contains( (float) event.getSceneX(), (float) event.getSceneY() ) ) {
            System.out.println( "Settings coming soon!" );
        }
    }
}
