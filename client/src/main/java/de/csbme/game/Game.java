package de.csbme.game;

import de.csbme.game.object.*;
import de.csbme.netty.NettyClient;
import de.csbme.player.Player;
import de.csbme.window.Window;
import lombok.Getter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Game {

    @Getter
    private static Game instance;

    @Getter
    private final Window window;

    @Getter
    private final CookieObject cookieObject;

    @Getter
    private final SettingsObject settingsObject;

    @Getter
    private final ShopObject shopObject;

    @Getter
    private final CookieCounterObject cookieCounterObject;

    @Getter
    private final SidebarObject sidebarObject;

    @Getter
    private Player player;

    @Getter
    private Properties data;

    @Getter
    private File dataFile;

    @Getter
    private NettyClient nettyClient;

    @Getter
    private ExecutorService pool = Executors.newCachedThreadPool();

    @Getter
    private List<Player> topTenPlayers = new LinkedList<>();

    @Getter
    private Thread mainThread;

    public Game( Window window ) {
        Game.instance = this;
        this.mainThread = Thread.currentThread();

        this.player = new Player( true );
        this.window = window;
        this.cookieObject = new CookieObject();
        this.cookieCounterObject = new CookieCounterObject();
        this.settingsObject = new SettingsObject();
        this.shopObject = new ShopObject();
        this.sidebarObject = new SidebarObject();

        // Set username
        try {
            this.getPlayer().setUsername( InetAddress.getLocalHost().getHostName() );
        } catch ( UnknownHostException e ) {
            e.printStackTrace();
        }

        // Load Data
        this.loadData();

        // Initialize and connect netty client
        this.nettyClient = new NettyClient( "127.0.0.1", 19132 );
        this.nettyClient.connectClient();

        Runtime.getRuntime().addShutdownHook( new Thread( () -> {
            this.getData().setProperty( "cookies", String.valueOf( this.getPlayer().getCookies() ) );

            try {
                this.getData().store( new FileOutputStream( this.getDataFile() ), "Do not change anything here!" );
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        } ) );
    }

    private void loadData() {
        String tempPath = System.getProperty( "java.io.tmpdir" );
        File folder = new File( tempPath + "/CookieClicker" );
        if ( !folder.exists() ) {
            if ( !folder.mkdir() ) {
                System.out.println( "Datafolder cant created!" );
                return;
            }
        }

        // Init Data
        File file = new File( folder, "data.properties" );
        Properties properties = new Properties();
        if ( !file.exists() ) {
            properties.setProperty( "cookies", "0" );

            try {
                properties.store( new FileOutputStream( file ), "Do not change anything here!" );
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }

        try {
            properties.load( new FileInputStream( file ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        this.data = properties;
        this.dataFile = file;

        // Load Data
        if ( this.getData().containsKey( "uniqueId" ) ) {
            this.getPlayer().setUniqueId( UUID.fromString( this.getData().getProperty( "uniqueId" ) ) );
        }

        this.getPlayer().setCookies( Long.valueOf( this.getData().getProperty( "cookies" ) ) );
        this.getCookieCounterObject().update();
    }

    public void setUniqueId( UUID uuid ) {
        this.getPlayer().setUniqueId( uuid );
        this.getData().setProperty( "uniqueId", uuid.toString() );

        try {
            this.getData().store( new FileOutputStream( this.getDataFile() ), "Do not change anything here!" );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }
}
