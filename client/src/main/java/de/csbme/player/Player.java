package de.csbme.player;

import de.csbme.game.Game;
import de.csbme.packet.PacketType;
import de.csbme.packet.packets.UpdateCookiePacket;
import lombok.Getter;
import lombok.Setter;

import java.util.*;

public class Player {

    @Getter
    @Setter
    private UUID uniqueId = null;

    @Getter
    private boolean self = false;

    @Getter
    @Setter
    private String username = "";

    @Getter
    @Setter
    private long cookies = 0;

    @Getter
    @Setter
    private long cookiesPerClick = 1;

    @Getter
    private List<Player> friends = new ArrayList<>();

    public Player( boolean self ) {
        this.self = self;

        if ( this.isSelf() ) {
            new Timer().schedule( new TimerTask() {
                @Override
                public void run() {
                    if ( Player.this.getUniqueId() == null || Game.getInstance().getNettyClient() == null ) {
                        return;
                    }

                    // Update Cookies
                    UpdateCookiePacket updateCookiePacket = new UpdateCookiePacket( Player.this.getUniqueId(), Player.this.getCookies() );
                    Game.getInstance().getNettyClient().sendPacket( PacketType.UPDATE_COOKIE_PACKET, updateCookiePacket );
                }
            }, 100, 100 );
        }
    }
}
