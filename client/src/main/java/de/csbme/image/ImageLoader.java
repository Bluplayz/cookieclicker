package de.csbme.image;

import javafx.scene.image.Image;

import java.util.HashMap;
import java.util.Map;

public class ImageLoader {

    private static Map<String, Image> images = new HashMap<>();

    public static Image load( String path ) {
        if ( ImageLoader.images.containsKey( path ) ) {
            return ImageLoader.images.get( path );
        }

        Image image = new Image( ImageLoader.class.getClassLoader().getResourceAsStream( path ) );
        ImageLoader.images.put( path + image.getWidth() + "x" + image.getHeight(), image );

        if ( image.getException() != null ) {
            image.getException().printStackTrace();
        }

        return image;
    }

    public static Image load( String path, int width, int height ) {
        if ( ImageLoader.images.containsKey( path + width + "x" + height ) ) {
            return ImageLoader.images.get( path + width + "x" + height );
        }

        Image image = new Image( ImageLoader.class.getClassLoader().getResourceAsStream( path ), width, height, true, true );
        ImageLoader.images.put( path + width + "x" + height, image );

        if ( image.getException() != null ) {
            image.getException().printStackTrace();
        }

        return image;
    }
}
