package de.csbme.netty;

import de.csbme.packet.InitPacket;
import de.csbme.packet.PacketType;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import lombok.Getter;

public class NettyClient {

    private NettyClient instance;

    private String host;

    private int port;

    @Getter
    private ClientHandler clientHandler;

    public NettyClient( String host, int port ) {
        this.instance = this;
        this.host = host;
        this.port = port;
    }

    /**
     * Send a packet to a client
     *
     * @param packetType of the packet
     * @param packet     to send
     * @param channel    to send
     */
    public void sendPacket( PacketType packetType, Object packet, Channel channel ) {
        channel.writeAndFlush( new InitPacket( packetType, packet ) );
//        System.out.println( "Packet " + packet.getClass().getSimpleName() + " output" );
    }

    /**
     * Send a packet to a client
     *
     * @param packetType of the packet
     * @param packet     to send
     */
    public void sendPacket( PacketType packetType, Object packet ) {
        if ( this.getClientHandler() == null || this.getClientHandler().getChannel() == null ) {
            return;
        }

        this.sendPacket( packetType, packet, this.getClientHandler().getChannel() );
    }

    /**
     * Connect the client to the server
     */
    public void connectClient() {
        new Thread( () -> {
            EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
            try {
                Bootstrap bootstrap = new Bootstrap();
                bootstrap.group( eventLoopGroup )
                        .channel( NioSocketChannel.class )
                        .handler( new ChannelInitializer<SocketChannel>() {
                            @Override
                            protected void initChannel( SocketChannel socketChannel ) throws Exception {
                                socketChannel.pipeline().addLast( new ObjectDecoder( Integer.MAX_VALUE, ClassResolvers.cacheDisabled( getClass().getClassLoader() ) ) );
                                socketChannel.pipeline().addLast( new ObjectEncoder() );
                                socketChannel.pipeline().addLast( NettyClient.this.clientHandler = new ClientHandler( instance ) );
                            }
                        } );

                ChannelFuture channelFuture = bootstrap.connect( this.host, this.port );
                ChannelFuture closeFuture = channelFuture.channel().closeFuture();
                closeFuture.addListener( channelFuture1 -> {
                    System.out.println( "[Netty] No connection to the server! Reconnect in 3 seconds..." );

                    // Wait 3 seconds
                    Thread.sleep( 3000L );

                    // Connect client again
                    this.connectClient();

                    eventLoopGroup.shutdownGracefully();
                } );
                closeFuture.sync();
            } catch ( Exception e ) {
                e.printStackTrace();
            } finally {
                eventLoopGroup.shutdownGracefully();
            }
        } ).start();
    }
}
