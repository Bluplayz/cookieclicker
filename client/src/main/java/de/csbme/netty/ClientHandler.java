package de.csbme.netty;

import de.csbme.game.Game;
import de.csbme.packet.InitPacket;
import de.csbme.packet.PacketType;
import de.csbme.packet.packets.ClientLoginPacket;
import de.csbme.packet.packets.ReceiveUniqueIdPacket;
import de.csbme.packet.packets.TopTenPacket;
import de.csbme.player.Player;
import de.csbme.window.Window;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.ReferenceCountUtil;
import lombok.Getter;

import java.util.LinkedList;
import java.util.List;

public class ClientHandler extends SimpleChannelInboundHandler<Object> {

    private NettyClient nettyClient;

    @Getter
    private Channel channel = null;

    public ClientHandler( NettyClient nettyClient ) {
        this.nettyClient = nettyClient;
    }

    @Override
    protected void channelRead0( ChannelHandlerContext channelHandlerContext, Object packet ) throws Exception {
        try {
            InitPacket initPacket = (InitPacket) packet;

//            System.out.println( "Packet " + initPacket.getClass().getSimpleName() + " input" );

            switch ( initPacket.getPacketType() ) {
                case RECEIVE_UNIQUEID_PACKET: {
                    ReceiveUniqueIdPacket receiveUniqueIdPacket = (ReceiveUniqueIdPacket) initPacket.getPacket();

                    // Set unique id
                    Window.getInstance().getGame().setUniqueId( receiveUniqueIdPacket.getPlayer() );
                    return;
                }
                case TOP_TEN_PACKET: {
                    TopTenPacket topTenPacket = (TopTenPacket) initPacket.getPacket();

                    List<Player> list = new LinkedList<>();
                    for ( String[] playerStat : topTenPacket.getPlayers() ) {
                        String username = playerStat[0];
                        String cookies = playerStat[1];

                        Player player = new Player( false );
                        player.setUsername( username );
                        player.setCookies( Long.parseLong( cookies ) );

                        list.add( player );
                    }
                    Game.getInstance().getTopTenPlayers().clear();
                    Game.getInstance().getTopTenPlayers().addAll( list );

                    Game.getInstance().getSidebarObject().loadTopPlayers();
                }
                default:
                    return;
            }
        } catch ( Exception e ) {
            e.printStackTrace();
        } finally {
            ReferenceCountUtil.release( packet );
        }
    }

    @Override
    public void channelActive( ChannelHandlerContext ctx ) throws Exception {
        this.channel = ctx.channel();

        // Client connected to the server. Send the client login packet
        Game game = Window.getInstance().getGame();
        ClientLoginPacket clientLoginPacket = new ClientLoginPacket( game.getPlayer().getUniqueId(), game.getPlayer().getUsername(), game.getPlayer().getCookies() );
        this.nettyClient.sendPacket( PacketType.CLIENT_LOGIN_PACKET, clientLoginPacket, ctx.channel() );
    }

    @Override
    public void channelInactive( ChannelHandlerContext ctx ) throws Exception {
        // Empty
    }

    @Override
    public void exceptionCaught( ChannelHandlerContext ctx, Throwable cause ) throws Exception {
        //cause.printStackTrace();
    }
}
