package de.csbme.command.commands;

import de.csbme.command.Command;

public class StopCommand extends Command {

    public StopCommand() {
        super( "stop" );

        this.getAliases().add( "end" );

    }

    @Override
    public void execute( String label, String[] args ) {
        System.exit( 0 );
    }
}
