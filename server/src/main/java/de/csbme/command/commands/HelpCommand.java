package de.csbme.command.commands;

import de.csbme.MasterServer;
import de.csbme.command.Command;

public class HelpCommand extends Command {

    public HelpCommand() {
        super( "help" );

        this.getAliases().add( "?" );
        this.getAliases().add( "commands" );
    }

    @Override
    public void execute( String label, String[] args ) {
        System.out.println( "Commands:" );
        for ( Command command : MasterServer.getInstance().getCommandHandler().getCommands() ) {
            if ( command.getAliases().size() > 0 ) {
                System.out.println( "- " + command.getName() + " (Aliases: " + command.getAliases().toString().replace( "[", "" ).replace( "]", "" ) + ")" );
                continue;
            }
            System.out.println( "- " + command.getName() );
        }
    }
}
