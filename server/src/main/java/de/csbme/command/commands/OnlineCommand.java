package de.csbme.command.commands;

import de.csbme.MasterServer;
import de.csbme.command.Command;
import de.csbme.player.User;

public class OnlineCommand extends Command {

    public OnlineCommand() {
        super( "online" );

        this.getAliases().add( "players" );
        this.getAliases().add( "users" );
    }

    @Override
    public void execute( String label, String[] args ) {
        System.out.println( "OnlinePlayers:" );
        for ( User user : MasterServer.getInstance().getUserManager().getUsers().values() ) {
            if ( !user.isOnline() ) continue;
            System.out.println( "- " + user.getPlayer() + " (" + user.getUsername() + ")" );
        }
    }
}
