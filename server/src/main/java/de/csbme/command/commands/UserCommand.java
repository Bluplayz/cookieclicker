package de.csbme.command.commands;

import de.csbme.MasterServer;
import de.csbme.command.Command;
import de.csbme.player.User;

import java.util.UUID;

public class UserCommand extends Command {

    public UserCommand() {
        super( "user" );

        this.getAliases().add( "userinfo" );
        this.getAliases().add( "player" );
    }

    @Override
    public void execute( String label, String[] args ) {
        if ( args.length != 1 ) {
            System.out.println( "- Usage: /user <UUID|Username>" );
            return;
        }
        User user;
        try {
            // Check if the input is an uuid
            UUID uuid = UUID.fromString( args[0] );

            // Get user by uuid
            user = MasterServer.getInstance().getUserManager().getUser( uuid );
        } catch ( IllegalArgumentException e ) {
            // Input is a username
            String username = args[0];

            // Get user by username
            user = MasterServer.getInstance().getUserManager().getUser( username );
        }
        // Check if user exists
        if ( user == null ) {
            System.out.println( "- User not exists." );
            return;
        }

        System.out.println( "- User '" + user.getPlayer() + "':" );
        System.out.println( "  - Name: " + user.getUsername() );
        System.out.println( "  - Cookies: " + user.getCookies() );
        System.out.println( "  - Ranking Position: #" + user.getRank() );
        if ( user.isOnline() ) {
            System.out.println( "  - Online: Yes" );
            System.out.println( "  - IP-Address: " + user.getAdress() );
        } else {
            System.out.println( "  - Online: No" );
            System.out.println( "  - LastOnline: " + user.getLastOnline() );
        }
        System.out.println( "  - Logins: " + user.getLogins() );
        System.out.println( "  - OnlineTime: " + user.getFullOnlineTime() );
        return;
    }
}
