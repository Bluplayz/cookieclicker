package de.csbme.command;

import lombok.Getter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.function.Consumer;

public class CommandHandler {

    @Getter
    public ArrayList<Command> commands = new ArrayList<>();

    /**
     * Register a command
     *
     * @param command to register
     */
    public void registerCommand( Command command ) {
        for ( Command cmd : getCommands() ) {
            if ( command.getName().equalsIgnoreCase( cmd.getName() ) ) {
                System.out.println( "[CommandHandler] The name of the command '" + command.getName() + "' is already in use." );
                return;
            }

            if ( cmd.getAliases().contains( command.getName() ) ) {
                System.out.println( "[CommandHandler] An alias is already registered with the name " + command.getName() + "." );
                return;
            }

            for ( String alias : command.getAliases() ) {
                if ( cmd.getAliases().contains( alias ) ) {
                    System.out.println( "The alias '" + alias + "' is already an alias of an other command." );
                    return;
                }
            }
        }

        getCommands().add( command );
    }

    /**
     * Execute a command
     *
     * @param message to execute
     * @return the command if the command not exists
     */
    public String onExecute( String message ) {
        String commandName = message.split( " " )[0];
        Command command = getCommandByName( commandName );

        if ( command == null ) {
            return commandName;
        }

        ArrayList<String> args = new ArrayList<>();
        for ( String argument : message.substring( commandName.length() ).split( " " ) ) {
            if ( argument.equalsIgnoreCase( "" ) || argument.equalsIgnoreCase( " " ) ) {
                continue;
            }

            args.add( argument );
        }

        command.execute( commandName, args.toArray( new String[args.size()] ) );
        return null;
    }

    /**
     * Get a command by name
     *
     * @param name of the command
     * @return the command. the result can be null if the command not exists
     */
    public Command getCommandByName( String name ) {
        for ( Command command : getCommands() ) {
            if ( command.getName().equalsIgnoreCase( name ) ) {
                return command;
            }

            for ( String alias : command.getAliases() ) {
                if ( alias.equalsIgnoreCase( name ) ) {
                    return command;
                }
            }
        }

        return null;
    }

    /**
     * Check if a command exists
     *
     * @param command name to check
     * @return if the command exists
     */
    public boolean commandExist( String command ) {
        return getCommandByName( command ) != null;
    }

    /**
     * Execute the console input
     *
     * @param consumer with the command when it not exists
     */
    public void consoleInput( Consumer<String> consumer ) {
        try {
            BufferedReader br = new BufferedReader( new InputStreamReader( System.in ) );
            String input;
            while ( ( input = br.readLine() ) != null ) {
                if ( !input.isEmpty() ) {
                    String commandName = this.onExecute( input );
                    if ( commandName != null ) {
                        consumer.accept( this.onExecute( commandName ) );
                    }
                }
            }
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        this.consoleInput( consumer );
    }
}
