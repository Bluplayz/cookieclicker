package de.csbme;

import de.csbme.command.CommandHandler;
import de.csbme.command.commands.HelpCommand;
import de.csbme.command.commands.OnlineCommand;
import de.csbme.command.commands.StopCommand;
import de.csbme.command.commands.UserCommand;
import de.csbme.database.MySQL;
import de.csbme.netty.NettyServer;
import de.csbme.player.UserManager;
import de.csbme.player.UserRepository;
import de.csbme.player.stats.StatsManager;
import lombok.Getter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class MasterServer {

    @Getter
    private static MasterServer instance;

    @Getter
    private MySQL mySQL;

    @Getter
    private UserRepository userRepository;

    @Getter
    private UserManager userManager;

    @Getter
    private NettyServer nettyServer;

    @Getter
    private CommandHandler commandHandler;

    @Getter
    private StatsManager statsManager;

    /**
     * TODO
     * - Change user name + no duplicate username
     * - Friends (+ Requests)
     * - Upgrades
     * - Shop
     * - User icons
     * - Cookies check on join (cheating)
     */

    public MasterServer() {
        MasterServer.instance = this;
        System.out.println( "Starting MasterServer..." );

        // Rename main thread
        Thread.currentThread().setName( "MasterServer-Main" );

        // Load config for mysql & netty
        Properties properties = getMasterConfig();

        // Create mysql connection
        this.mySQL = new MySQL( properties.getProperty( "mysql.host" ), properties.getProperty( "mysql.database" ),
                properties.getProperty( "mysql.user" ), properties.getProperty( "mysql.password" ), Integer.parseInt( properties.getProperty( "mysql.port" ) ) );

        // Initialize user repository & manager
        this.userRepository = new UserRepository( this.mySQL );
        this.userManager = new UserManager( this.userRepository );

        // Initialize and start netty server
        this.nettyServer = new NettyServer( Integer.parseInt( properties.getProperty( "netty.port" ) ) );
        new Thread( this.nettyServer ).start();

        // Initialize command handler
        this.commandHandler = new CommandHandler();

        // Register commands
        this.registerCommands();

        // Initialize console input
        this.userRepository.getPool().execute( () -> {
            Thread.currentThread().setName( "MasterServer-Console" );

            this.commandHandler.consoleInput( command -> System.out.println( "[CommandHandler] The command '" + command + "' not exists." ) );
        } );

        // Initialize stats manager
        this.statsManager = new StatsManager();

        // Add shutdown hook
        Runtime.getRuntime().addShutdownHook( new Thread( () -> {
            if ( this.mySQL.isConnected() ) {

                // Save player data
                MasterServer.this.getUserManager().savePlayers( false );

                if ( MasterServer.this.getMySQL().disconnect() ) {
                    System.out.println( "[MySQL] Connection successfully closed!" );
                }
            }
        } ) );
    }

    /**
     * Get the master properties with mysql connection data and the netty port
     *
     * @return the properties file
     */
    private Properties getMasterConfig() {
        File file = new File( "config.properties" );

        Properties properties = new Properties();
        if ( !file.exists() ) {
            properties.setProperty( "netty.port", "19132" );
            properties.setProperty( "mysql.host", "localhost" );
            properties.setProperty( "mysql.database", "db" );
            properties.setProperty( "mysql.user", "root" );
            properties.setProperty( "mysql.password", "pw" );
            properties.setProperty( "mysql.port", "3306" );

            // Save properties
            try {
                properties.store( new FileOutputStream( file ), "Master Config (MySQL & Netty)" );
            } catch ( IOException e ) {
                e.printStackTrace();
            }
            return properties;
        }
        // Load properties
        try {
            properties.load( new FileInputStream( file ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
        return properties;
    }

    /**
     * Register the commands for the master server
     */
    private void registerCommands() {
        this.commandHandler.registerCommand( new HelpCommand() );
        this.commandHandler.registerCommand( new UserCommand() );
        this.commandHandler.registerCommand( new OnlineCommand() );
        this.commandHandler.registerCommand( new StopCommand() );
    }

    /**
     * Main method
     */
    public static void main( String[] args ) {
        new MasterServer();
    }
}
