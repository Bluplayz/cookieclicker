package de.csbme.player;

import io.netty.channel.Channel;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.UUID;

public class User implements Comparable<User> {

    @Getter
    private UUID player;

    @Getter
    private String username;

    @Getter
    private long cookies, logins, onlineTime;

    @Getter
    @Setter
    private int rank;

    @Getter
    private Timestamp lastOnline;

    @Getter
    @Setter
    private Channel channel;

    @Getter
    @Setter
    private long loginTime;

    @Getter
    private boolean update = false;

    private UserRepository userRepository;

    public User( UUID uuid, String username, long cookies, long logins, long onlineTime, Timestamp lastOnline, UserRepository userRepository ) {
        this.player = uuid;
        this.username = username;
        this.cookies = cookies;
        this.logins = logins;
        this.onlineTime = onlineTime;
        this.lastOnline = lastOnline;
        this.userRepository = userRepository;
    }

    public User( UUID uuid, String username, UserRepository userRepository ) {
        this( uuid, username, 0, 1, 0, new Timestamp( System.currentTimeMillis() ), userRepository );
    }

    /**
     * Check if a user is online
     */
    public boolean isOnline() {
        return this.channel != null;
    }

    /**
     * Get the address the user
     *
     * @return the address
     */
    public String getAdress() {
        if ( this.channel == null ) {
            return "Unbekannt";
        }
        return this.channel.remoteAddress().toString().substring( 1 );
    }

    /**
     * Save player data
     *
     * @param asyncUpdate the player data
     */
    public void updatePlayerData( boolean asyncUpdate ) {
        if ( this.loginTime == 0 && !this.update ) return;
        long current = System.currentTimeMillis();
        long diff = current - this.loginTime;
        this.loginTime = current;
        this.onlineTime += diff;
        this.userRepository.updatePlayerData( this.player, this.cookies, this.logins, this.onlineTime, this.lastOnline, asyncUpdate );
        this.update = false;
    }

    /**
     * Insert player data
     */
    public void insertPlayerData() {
        this.userRepository.insertPlayerData( this.player, this.username );
    }

    /**
     * Show the online time as string
     *
     * @return the online time as string
     */
    public String getFullOnlineTime() {
        if ( this.loginTime != 0 ) {
            long current = System.currentTimeMillis();
            long diff = current - this.loginTime;
            this.loginTime = current;
            this.onlineTime += diff;
        }
        return this.userRepository.getOnlineTimeAsString( this.onlineTime );
    }

    /**
     * Set cookies
     *
     * @param cookies to set
     */
    public void setCookies( long cookies ) {
        if ( this.cookies == cookies ) return;
        this.cookies = cookies;
        if ( !this.update ) {
            this.update = true;
        }
    }

    /**
     * Set logins
     *
     * @param logins to set
     */
    public void setLogins( long logins ) {
        if ( this.logins == logins ) return;
        this.logins = logins;
        if ( !this.update ) {
            this.update = true;
        }
    }

    /**
     * Set last online
     *
     * @param lastOnline online to set
     */
    public void setLastOnline( Timestamp lastOnline ) {
        if ( this.lastOnline == lastOnline ) return;
        this.lastOnline = lastOnline;
        if ( !this.update ) {
            this.update = true;
        }
    }

    @Override
    public int compareTo( User user ) {
        return Long.compare( user.getCookies(), this.cookies );
    }
}
