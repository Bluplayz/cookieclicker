package de.csbme.player;

import de.csbme.database.MySQL;
import lombok.Getter;

import java.sql.*;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class UserRepository {

    private MySQL mySQL;

    // PreparedStatements for the player data
    private PreparedStatement selectAllPlayers, updatePlayerData, insertPlayerData, selectUsername;

    @Getter
    private ExecutorService pool = Executors.newCachedThreadPool();

    public UserRepository( MySQL mySQL ) {
        this.mySQL = mySQL;

        Connection connection = this.mySQL.getConnection();
        try {
            // Create PreparedStatements for player data
            this.selectAllPlayers = connection.prepareStatement( "SELECT * FROM " + mySQL.getTABLE_NAME() );
            this.updatePlayerData = connection.prepareStatement( "UPDATE " + mySQL.getTABLE_NAME() + " SET cookies=?,logins=?,onlineTime=?,lastonline=? WHERE player=?" );
            this.insertPlayerData = connection.prepareStatement( "INSERT INTO " + mySQL.getTABLE_NAME() + " (player, username) VALUES(?,?)" );
            this.selectUsername = connection.prepareStatement( "SELECT username FROM " + mySQL.getTABLE_NAME() + " WHERE player=?" );
        } catch ( SQLException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Check if a player is exist
     *
     * @param uuid     of the player
     * @param consumer with the result
     */
    public void isPlayerExists( UUID uuid, Consumer<Boolean> consumer ) {
        this.pool.execute( () -> {
            try {
                PreparedStatement preparedStatement = this.selectUsername;
                preparedStatement.setString( 1, uuid.toString() );
                ResultSet resultSet = preparedStatement.executeQuery();
                if ( resultSet.next() ) {
                    // Player found
                    consumer.accept( true );
                    return;
                }
            } catch ( SQLException e ) {
                e.printStackTrace();
            }
            consumer.accept( false );
        } );
    }

    /**
     * Get all players
     *
     * @param consumer with the player data
     */
    public void getAllPlayers( Consumer<ResultSet> consumer ) {
        try {
            PreparedStatement preparedStatement = this.selectAllPlayers;
            ResultSet resultSet = preparedStatement.executeQuery();
            consumer.accept( resultSet );
            return;
        } catch ( SQLException e ) {
            e.printStackTrace();
        }
        consumer.accept( null );
    }

    /**
     * Insert a player into the database when the player not exists
     *
     * @param uuid     of the player
     * @param username of the player
     */
    public void insertPlayerData( UUID uuid, String username ) {
        this.pool.execute( () -> {
            try {
                PreparedStatement preparedStatement = this.insertPlayerData;
                preparedStatement.setString( 1, uuid.toString() );
                preparedStatement.setString( 2, username );
                preparedStatement.executeUpdate();
            } catch ( SQLException e ) {
                e.printStackTrace();
            }
        } );
    }

    /**
     * Update player data
     *
     * @param player      to save
     * @param cookies     to save
     * @param logins      to save
     * @param onlineTime  to save
     * @param lastonline  to save
     * @param asyncUpdate the player data
     */
    public void updatePlayerData( UUID player, long cookies, long logins, long onlineTime, Timestamp lastonline, boolean asyncUpdate ) {
        if ( asyncUpdate ) {
            this.pool.execute( () -> {
                try {
                    PreparedStatement preparedStatement = this.updatePlayerData;
                    preparedStatement.setLong( 1, cookies );
                    preparedStatement.setLong( 2, logins );
                    preparedStatement.setLong( 3, onlineTime );
                    preparedStatement.setTimestamp( 4, lastonline );
                    preparedStatement.setString( 5, player.toString() );
                    preparedStatement.executeUpdate();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            } );
            return;
        }
        try {
            PreparedStatement preparedStatement = this.updatePlayerData;
            preparedStatement.setLong( 1, cookies );
            preparedStatement.setLong( 2, logins );
            preparedStatement.setLong( 3, onlineTime );
            preparedStatement.setTimestamp( 4, lastonline );
            preparedStatement.setString( 5, player.toString() );
            preparedStatement.executeUpdate();
        } catch ( SQLException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Convert the online time to a string
     *
     * @param onlineTime to convert
     * @return the online time as string
     */
    public String getOnlineTimeAsString( long onlineTime ) {
        int time = (int) ( onlineTime / 1000 );
        int hours = ( time % 86400 ) / 3600;
        int minutes = ( time % 3600 ) / 60;
        int seconds = time % 60;

        return hours + "h:" + minutes + "m:" + seconds + "s";
    }
}
