package de.csbme.player.stats;

import de.csbme.MasterServer;
import de.csbme.packet.PacketType;
import de.csbme.packet.packets.TopTenPacket;
import de.csbme.player.User;
import lombok.Getter;

import java.util.*;

public class StatsManager {

    @Getter
    private TopTenPacket topTenPacket;

    public StatsManager() {
        new Timer().schedule( new TimerTask() {
            @Override
            public void run() {
                // Get all players
                List<User> users = new ArrayList<>( MasterServer.getInstance().getUserManager().getUsers().values() );
                List<String[]> topTenPlayers = new ArrayList<>();

                // Sort players
                Collections.sort( users );

                int rank = 1;
                for ( User user : users ) {
                    user.setRank( rank );
                    if ( rank <= 10 ) {
                        topTenPlayers.add( new String[]{ user.getUsername(), String.valueOf( user.getCookies() ) } );
                    }
                    rank++;
                }
                StatsManager.this.topTenPacket = new TopTenPacket( topTenPlayers );

                // Send top ten to all online players
                for ( User user : MasterServer.getInstance().getUserManager().getUsers().values() ) {
                    if ( !user.isOnline() ) continue;
                    MasterServer.getInstance().getNettyServer().sendPacket( PacketType.TOP_TEN_PACKET, StatsManager.this.topTenPacket, user.getChannel() );
                }
            }
        }, 100L, 100L ); // 0,1 seconds
    }
}
