package de.csbme.player;

import io.netty.channel.Channel;
import lombok.Getter;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.function.Consumer;

public class UserManager {

    @Getter
    private HashMap<UUID, User> users = new HashMap<>();

    @Getter
    private UserRepository userRepository;

    public UserManager( UserRepository userRepository ) {
        this.userRepository = userRepository;

        this.userRepository.getPool().execute( () -> {
            this.userRepository.getAllPlayers( resultSet -> {
                if ( resultSet == null ) return;
                try {
                    while ( resultSet.next() ) {
                        // Cache player data
                        User user = new User( UUID.fromString( resultSet.getString( "player" ) ), resultSet.getString( "username" ),
                                resultSet.getLong( "cookies" ), resultSet.getLong( "logins" ), resultSet.getLong( "onlineTime" ),
                                resultSet.getTimestamp( "lastonline" ), this.userRepository );
                        this.users.put( user.getPlayer(), user );
                    }
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            } );
            System.out.println( "[UserManager] Loaded " + this.users.size() + " Players!" );
        } );

        new Timer().schedule( new TimerTask() {
            @Override
            public void run() {
                UserManager.this.savePlayers( false );
            }
        }, 180000L, 180000L ); // 3 minutes
    }

    /**
     * Get a user by uuid
     *
     * @param uuid of the player
     * @return the user. the result can be null if the user not exists
     */
    public User getUser( UUID uuid ) {
        return this.users.getOrDefault( uuid, null );
    }

    /**
     * Get a user by username
     *
     * @param username of the player
     * @return the user. the result can be null if the user not exists
     */
    public User getUser( String username ) {
        for ( User user : this.users.values() ) {
            if ( !user.getUsername().equalsIgnoreCase( username ) ) continue;
            return user;
        }
        return null;
    }

    /**
     * Get a user with the netty channel
     *
     * @param channel of the player
     * @return the user. the result can be null if the user not exists
     */
    public User getUser( Channel channel ) {
        for ( User user : this.users.values() ) {
            if ( !user.isOnline() ) continue;
            if ( !user.getChannel().equals( channel ) ) continue;
            return user;
        }
        return null;
    }

    /**
     * Create a user
     *
     * @param uuid     of the player
     * @param username of the player
     * @return the created user
     */
    public User createUser( UUID uuid, String username ) {
        User user = new User( uuid, username, this.userRepository );
        user.insertPlayerData();
        this.users.put( uuid, user );
        return user;
    }

    /**
     * Save all players with changed stats
     *
     * @param asyncUpdate the player data
     */
    public void savePlayers( boolean asyncUpdate ) {
        for ( User user : this.users.values() ) {
            user.updatePlayerData( asyncUpdate );
        }
    }

    /**
     * Generate a new uuid for a player
     *
     * @param consumer with the generated uuid
     */
    public void generateUuid( Consumer<UUID> consumer ) {
        UUID generatedUuid = UUID.randomUUID();

        // Check if uuid already exists
        this.userRepository.isPlayerExists( generatedUuid, exist -> {
            if ( exist ) {
                // Uuid exists. Generating a new uuid
                generateUuid( consumer );
                return;
            }
            consumer.accept( generatedUuid );
        } );
    }
}
