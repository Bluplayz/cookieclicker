package de.csbme.netty;

import de.csbme.MasterServer;
import de.csbme.packet.InitPacket;
import de.csbme.packet.PacketType;
import de.csbme.packet.packets.ClientLoginPacket;
import de.csbme.packet.packets.ReceiveUniqueIdPacket;
import de.csbme.packet.packets.UpdateCookiePacket;
import de.csbme.packet.packets.UpdateUsernamePacket;
import de.csbme.player.User;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.ReferenceCountUtil;

import java.sql.Timestamp;

public class ServerHandler extends SimpleChannelInboundHandler<Object> {

    private NettyServer nettyServer;

    public ServerHandler( NettyServer nettyServer ) {
        this.nettyServer = nettyServer;
    }

    @Override
    protected void channelRead0( ChannelHandlerContext channelHandlerContext, Object packet ) throws Exception {
        try {
            InitPacket initPacket = (InitPacket) packet;
            Channel channel = channelHandlerContext.channel();

            switch ( initPacket.getPacketType() ) {
                case CLIENT_LOGIN_PACKET: {
                    ClientLoginPacket clientLoginPacket = (ClientLoginPacket) initPacket.getPacket();

                    // Check if the player not exists
                    if ( clientLoginPacket.getPlayer() == null ) {
                        // Player not exists. Generate a new Uuid
                        MasterServer.getInstance().getUserManager().generateUuid( uuid -> {
                            // Create user
                            User user = MasterServer.getInstance().getUserManager().createUser( uuid, clientLoginPacket.getUsername() );

                            // Set netty channel to user
                            user.setChannel( channel );

                            // Set cookies
                            user.setCookies( clientLoginPacket.getCookies() );

                            // Set login time
                            user.setLoginTime( System.currentTimeMillis() );

                            System.out.println( "[Network] User " + uuid + " logged in for the first time!" );

                            // Send the uuid to the client
                            this.nettyServer.sendPacket( PacketType.RECEIVE_UNIQUEID_PACKET, new ReceiveUniqueIdPacket( uuid ), channel );

                            // Send top ten packet
                            this.nettyServer.sendPacket( PacketType.TOP_TEN_PACKET, MasterServer.getInstance().getStatsManager().getTopTenPacket(), channel );
                        } );
                        return;
                    }

                    // Get user
                    User user = MasterServer.getInstance().getUserManager().getUser( clientLoginPacket.getPlayer() );

                    // Check if user exists
                    if ( user == null ) {
                        System.out.println( "[Network] Can't find player " + clientLoginPacket.getPlayer() + "!" );
                        return;
                    }

                    // Set netty channel to user
                    user.setChannel( channel );

                    // Set cookies
                    user.setCookies( clientLoginPacket.getCookies() );

                    // Set login time
                    user.setLoginTime( System.currentTimeMillis() );

                    // Set logins
                    user.setLogins( user.getLogins() + 1 );

                    // Send top ten packet
                    this.nettyServer.sendPacket( PacketType.TOP_TEN_PACKET, MasterServer.getInstance().getStatsManager().getTopTenPacket(), channel );

                    System.out.println( "[Network] User " + clientLoginPacket.getPlayer() + " logged in!" );
                    return;
                }
                case UPDATE_COOKIE_PACKET: {
                    UpdateCookiePacket updateCookiePacket = (UpdateCookiePacket) initPacket.getPacket();

                    // Get user
                    User user = MasterServer.getInstance().getUserManager().getUser( updateCookiePacket.getPlayer() );

                    // Set cookies
                    user.setCookies( updateCookiePacket.getCookies() );
                    return;
                }
                case UPDATE_USERNAME_PACKET: {
                    UpdateUsernamePacket updateUsernamePacket = (UpdateUsernamePacket) initPacket.getPacket();

                    // TODO Check if username is not used and save the username to the database
                }
                default:
            }
        } catch ( Exception e ) {
            e.printStackTrace();
        } finally {
            ReferenceCountUtil.release( packet );
        }
    }

    @Override
    public void channelActive( ChannelHandlerContext ctx ) throws Exception {
        // Empty
    }

    @Override
    public void channelInactive( ChannelHandlerContext ctx ) throws Exception {
        // Get the user from channel
        User user = MasterServer.getInstance().getUserManager().getUser( ctx.channel() );

        // Check if user exists
        if ( user != null ) {

            // Remove channel
            user.setChannel( null );

            // Set last online
            user.setLastOnline( new Timestamp( System.currentTimeMillis() ) );

            // Update player data
            user.updatePlayerData( true );

            // Set login time to 0
            user.setLoginTime( 0 );

            System.out.println( "[Network] User " + user.getPlayer() + " logged out!" );
        }
    }

    @Override
    public void exceptionCaught( ChannelHandlerContext ctx, Throwable cause ) throws Exception {
        //cause.printStackTrace();
    }
}
