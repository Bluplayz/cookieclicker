package de.csbme.netty;

import de.csbme.packet.InitPacket;
import de.csbme.packet.PacketType;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

public class NettyServer implements Runnable {

    private NettyServer instance;

    private int port;

    public NettyServer( int port ) {
        this.instance = this;
        this.port = port;
    }

    /**
     * Send a packet to a client
     *
     * @param packetType of the packet
     * @param packet     to send
     * @param channel    to send
     */
    public void sendPacket( PacketType packetType, Object packet, Channel channel ) {
        channel.writeAndFlush( new InitPacket( packetType, packet ) );
    }

    @Override
    public void run() {
        System.out.println( "[Netty] Starting server..." );
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group( bossGroup, workerGroup )
                    .channel( NioServerSocketChannel.class )
                    .childHandler( new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel( SocketChannel socketChannel ) throws Exception {
                            socketChannel.pipeline().addLast( new ObjectDecoder( Integer.MAX_VALUE, ClassResolvers.cacheDisabled( getClass().getClassLoader() ) ) );
                            socketChannel.pipeline().addLast( new ObjectEncoder() );
                            socketChannel.pipeline().addLast( new ServerHandler( instance ) );
                        }
                    } ).option( ChannelOption.SO_BACKLOG, Integer.MAX_VALUE );
            ChannelFuture channelFuture = serverBootstrap.bind( this.port ).sync();
            System.out.println( "[Netty] Server successfully started on Port " + this.port );
            channelFuture.channel().closeFuture().sync();
        } catch ( Exception e ) {
            System.out.println( "[Netty] Cannot start server!" );
            e.printStackTrace();
        }
    }
}
