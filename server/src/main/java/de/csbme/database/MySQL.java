package de.csbme.database;

import lombok.Getter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MySQL {

    @Getter
    private final String TABLE_NAME = "cookieclicker_playerdata";

    @Getter
    private Connection connection;

    @Getter
    private String host;

    @Getter
    private String database;

    @Getter
    private String user;

    @Getter
    private String password;

    @Getter
    private int port;

    public MySQL( String host, String database, String user, String password, int port ) {
        this.host = host;
        this.database = database;
        this.user = user;
        this.password = password;
        this.port = port;

        // Check Connection
        if ( this.connect() ) {
            System.out.println( "[MySQL] Connected!" );

            // Check tables
            this.checkTables();
        } else {
            System.exit( 0 );
        }
    }

    /**
     * Connect to the mysql database
     *
     * @return if the connection is ready
     */
    private boolean connect() {
        try {
            Class.forName( "com.mysql.jdbc.Driver" );

            this.connection = DriverManager.getConnection( "jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database + "?user=" +
                    this.user + "&password=" + this.password + "&autoReconnect=true" );
        } catch ( Exception e ) {
            System.out.println( "[MySQL] Can't connect to mysql!" );
            e.printStackTrace();
        }

        return this.isConnected();
    }

    /**
     * Disconnect from the mysql database
     *
     * @return if the connection was successfully closed
     */
    public boolean disconnect() {
        if ( !this.isConnected() ) {
            return true;
        }

        try {
            this.connection.close();
            return true;
        } catch ( SQLException e ) {
            System.out.println( "[MySQL] Can't close connection!" );
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Check if the tables exists. When not, create it
     */
    private void checkTables() {
        // Create Tables if not exists
        try {
            PreparedStatement preparedStatement = this.connection.prepareStatement( "CREATE TABLE IF NOT EXISTS " + this.TABLE_NAME + " (player VARCHAR(36), username VARCHAR(25), cookies BIGINT DEFAULT 0, logins BIGINT DEFAULT 1, onlinetime BIGINT DEFAULT 0, lastonline TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (player, username))" );
            preparedStatement.executeUpdate();
        } catch ( SQLException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Check if the connection is not null
     *
     * @return if the connection not null
     */
    public boolean isConnected() {
        return this.connection != null;
    }
}
